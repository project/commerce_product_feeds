<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_product_feeds\Kernel;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Cache\Cache;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests feed generation.
 *
 * @group commerce_product_feeds
 * @group commerce
 */
class FeedGenerationKernelTest extends CommerceKernelTestBase {

  /**
   * Test products for the generator.
   *
   * @var array
   */
  protected $testProducts = [
    [
      'product' => [
        'title' => 'Product #1 - single variation.',
        'status' => 1,
      ],
      'variations' => [
        [
          'price' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_1_1',
          'status' => 1,
          'title' => 'Product #1 - Variation #1 - 9.99',
        ],
      ],
    ],
    [
      'product' => [
        'title' => 'Product #2 - two variations.',
        'status' => 1,
      ],
      'variations' => [
        [
          'price' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_2_1',
          'status' => 1,
          'title' => 'Product #2 - Variation #1 - 9.99',
        ],
        [
          'price' => [
            'number' => '20.00',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_2_2',
          'status' => 1,
          'title' => 'Product #2 - Variation #2 - 20.00',
        ],
      ],
    ],
    [
      'product' => [
        'title' => 'Product #3 - two variations, one published.',
        'status' => 1,
      ],
      'variations' => [
        [
          'price' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_3_1',
          'status' => 1,
          'title' => 'Product #2 - Variation #1 - 9.99',
        ],
        [
          'price' => [
            'number' => '20.00',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_3_2',
          'status' => 0,
          'title' => 'Product #2 - Variation #2 - 20.00 - Disabled',
        ],
      ],
    ],
    [
      'product' => [
        'title' => 'Product #4 - single variation, unpublished.',
        'status' => 0,
      ],
      'variations' => [
        [
          'price' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
          'sku' => 'test_product_4_1',
          'status' => 1,
          'title' => 'Product #1 - Variation #1 - 9.99',
        ],
      ],
    ],
  ];

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_product_feeds',
    'commerce_product',
    'serialization',
  ];

  /**
   * Feed Generator.
   *
   * @var \Drupal\commerce_product_feeds\FeedGenerator
   */
  protected $generator;

  /**
   * The anonymous role.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected $anonymousRole;

  /**
   * The normalized value cacher.
   *
   * @var \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacherInterface
   */
  protected $cacher;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installConfig(['commerce_product', 'user']);

    $this->cacher = $this->container->get('commerce_product_feeds.cache');
    $this->cacheTagsInvalidator = $this->container->get('cache_tags.invalidator');
    $this->generator = $this->container->get('commerce_product_feeds.generator');

    $this->anonymousRole = Role::load('anonymous');
  }

  public function testFeedGeneration(): void {
    // Generate the test products.
    iterator_to_array($this->productGenerator());

    libxml_use_internal_errors(true);

    // Anonymous user has no permission to view.
    $this->assertEmpty($this->generateAtom()->xpath('//x:entry'));

    // Grant anonymous users view permission.
    $this->drupalGrantPermissions($this->anonymousRole, ['view commerce_product']);
    // There should be four accessible product variations serialized.
    $this->assertCount(4, $this->generateAtom()->xpath('//x:entry'));
  }

  /**
   * Tests the feed generation cache metadata.
   *
   * @todo Test with non-permanent max-age.
   */
  public function testCacheMetadata(): void {
    // Ensure the product is accessible to an anonymous user.
    $this->drupalGrantPermissions($this->anonymousRole, ['view commerce_product']);

    // Generate a feed with a single product.
    $product = $this->productGenerator()->current();
    assert($product instanceof ProductInterface);
    $this->assertCount(1, $this->generateAtom()->xpath('//x:entry'));

    // Check the product cache item metadata.
    $item = $this->cacher->get($product);
    $this->assertEquals([
      'commerce_product:1',
      'commerce_product_feeds_normalization',
      'commerce_product_variation:1',
      'commerce_product_variation_feeds_normalization',
      'commerce_product_variation_view',
    ], $item->getCacheTags());
    $this->assertSame(Cache::PERMANENT, $item->getCacheMaxAge());

    // Check the variation cache item metadata.
    $variation = $product->getDefaultVariation();
    $item = $this->cacher->get($variation);
    $this->assertEquals([
      'commerce_product:1',
      'commerce_product_variation:1',
      'commerce_product_variation_feeds_normalization',
      'commerce_product_variation_view',
    ], $item->getCacheTags());
    $this->assertSame(Cache::PERMANENT, $item->getCacheMaxAge());
  }

  /**
   * Generate Atom 1.0 feed.
   *
   * @return \SimpleXMLElement
   *   SimpleXMLElement with namespace registered for xpath.
   */
  protected function generateAtom(): \SimpleXMLElement {
    $xml = $this->generator->generate('xml_atom');
    $sxe = new \SimpleXMLElement($xml);
    $sxe->registerXPathNamespace('x', 'http://www.w3.org/2005/Atom');
    return $sxe;
  }

  /**
   * Generator for test products.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface[]
   *   Generator of products.
   */
  protected function productGenerator(): \Generator {
    foreach ($this->testProducts as $productDefinition) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      $product = Product::create($productDefinition['product'] + ['type' => 'default']);
      $product->save();
      foreach ($productDefinition['variations'] as $variationDefinition) {
        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation */
        $productVariation = ProductVariation::create($variationDefinition + ['type' => 'default']);
        $productVariation->save();
        $product->addVariation($productVariation);
      }
      $product->save();
      yield $product;
    }
  }

}
