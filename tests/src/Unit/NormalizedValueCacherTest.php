<?php

namespace Drupal\Tests\commerce_product_feeds\Unit;

use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use \Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Provides unit tests for the normalized value cacher.
 *
 * @coversDefaultClass \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher
 *
 * @group commerce_product_feeds
 * @group commerce
 */
class NormalizedValueCacherTest extends UnitTestCase {

  /**
   * The normalized value cacher's backing cache prophecy object.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $cache;

  /**
   * The content entity prophecy object.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected $entity;

  /**
   * The normalized value cacher under test.
   *
   * @var \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher
   */
  protected $cacher;

  /**
   * The time service prophecy object.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy;
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->cache = $this->prophesize(CacheBackendInterface::class);
    $this->time = $this->prophesize(TimeInterface::class);
    $this->cacher = new NormalizedValueCacher($this->cache->reveal(), $this->time->reveal());

    $this->entity = $this->prophesize(ContentEntityInterface::class);
    $this->entity->getEntityTypeId()->willReturn('entity_test');
    $this->entity->id()->willReturn(42);
    // The cacher should view the cacheable normalization as the source of truth
    // for cacheability metadata: it shouldn't get it from the entity directly.
    $this->entity->getCacheContexts()->shouldNotBeCalled();
    $this->entity->getCacheTags()->shouldNotBeCalled();
    $this->entity->getCacheMaxAge()->shouldNotBeCalled();
  }

  /**
   * Tests the cacheability metadata used when setting an item.
   *
   * @covers ::set
   */
  public function testSetCacheability(): void {
    $cacheability = new CacheableMetadata();
    $cacheability->setCacheMaxAge(600);
    $cacheability->addCacheTags(['test1', 'test2']);
    $normalization = new CacheableNormalization($cacheability, ['field' => 'value']);

    // The max-age should be converted to a unix timestamp.
    $this->time->getRequestTime()->willReturn(1000);
    $this->cache->set(Argument::any(), ['field' => 'value'], 1600, ['test1', 'test2'])
      ->shouldBeCalledOnce();

    $this->cacher->set($normalization, $this->entity->reveal());
  }

  /**
   * Tests setting a cache item with a permanent max-age.
   *
   * @covers ::set
   */
  public function testSetCacheabilityPermanent(): void {
    $cacheability = new CacheableMetadata();
    $normalization = new CacheableNormalization($cacheability, []);

    $this->cache->set(Argument::any(), [], Cache::PERMANENT, [])
      ->shouldBeCalledOnce();

    $this->cacher->set($normalization, $this->entity->reveal());
  }

}
