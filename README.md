# Drupal Commerce Product Feeds
This module provides functionality to generate feeds appropriate for submission
to third-party services such as Google Merchant or Facebook Catalogs.

## Usage
```sh
drush product-feed:generate path/to/feed.xml
```

## Customization
Customize the normalization by subscribing to the event
`commerce_product_feeds.product_variation_normalize`.


## Technical notes
This module makes extensive use of the serialization system and the Symfony
serialization component to translate Drupal's content entities to structured
data. The authors owe a great deal of gratitude to the `json:api` module authors
and maintainers for providing a model for this type of operation.

### Sponsored Development Acknowledgments
Thanks to Sport Obermeyer, an early adopter of Drupal Commerce, for
sponsoring the initial development of this module.

&copy; Fruition Growth LLC and contributors. GPL-2 license.
