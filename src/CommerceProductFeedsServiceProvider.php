<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds;

use Drupal\commerce_product_feeds\DependencyInjection\Compiler\RegisterSerializationClassesCompilerPass;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

class CommerceProductFeedsServiceProvider implements ServiceProviderInterface {

  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new RegisterSerializationClassesCompilerPass());
  }

}
