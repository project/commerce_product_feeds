<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Event;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Event to facilitate modules to provide input on the normalization of
 * product variations; this work could be performed in a normalizer, however
 * events are a more typical and accessible entrypoint for implementing site
 * builders, and a custom normalizer remains an option if that is preferable.
 */
class ProductVariationNormalizeEvent extends Event implements RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The normalization context.
   *
   * @var array
   */
  protected $context;

  /**
   * The product variation.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $productVariation;

  /**
   * Serializer on the calling normalizer; e.g. for field values.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * Normalized value.
   *
   * @var array
   */
  protected $value;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $productVariation
   *   Product Variation.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param array $value
   *   The serialized value thus far.
   * @param array $context
   *   (optional) The normalization context.
   */
  public function __construct(ProductVariationInterface $productVariation, SerializerInterface $serializer, array $value, array $context = []) {
    $this->productVariation = $productVariation;
    $this->serializer = $serializer;
    $this->value = $value;
    $this->context = $context;
  }

  /**
   * Gets the normalization context array.
   *
   * @return array
   *   The normalization context.
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * Getter for the product variation.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  public function getProductVariation(): ProductVariationInterface {
    return $this->productVariation;
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   */
  public function getSerializer(): SerializerInterface {
    return $this->serializer;
  }

  /**
   * Get the normalized value.
   *
   * @return array
   */
  public function getValue(): array {
    return $this->value;
  }

  /**
   * Set the normalized value.
   *
   * @param array $value
   */
  public function setValue(array $value): void {
    $this->value = $value;
  }

}
