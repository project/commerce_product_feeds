<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Event;

use Symfony\Component\EventDispatcher\Event;

abstract class FeedDataSelectionEventBase extends Event {

  /**
   * Serialization format.
   *
   * @var string
   */
  protected $format;

  /**
   * Serialization context.
   *
   * @var array
   */
  protected $context;

  /**
   * Constructor.
   *
   * @param string $format
   *   Format for operation.
   * @param array $context
   *   Serialization/normalization context.
   */
  public function __construct(string $format, array $context) {
    $this->format = $format;
    $this->context = $context;
  }

}
