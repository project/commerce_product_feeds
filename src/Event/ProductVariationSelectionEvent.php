<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Event;

use Drupal\commerce_product\Entity\ProductInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Event for selecting product variations for the feed.
 *
 * In the case event subscribers determine the variation list, they are
 * responsible for selecting the variations with access control and status in
 * context. See the default selection implementation for an example.
 *
 * @see \Drupal\commerce_product_feeds\Normalizer\CommerceProductNormalizer::getVariations()
 */
class ProductVariationSelectionEvent extends FeedDataSelectionEventBase {

  /**
   * The product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * Product variations to include in the feed.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface[]
   */
  protected $variations = [];

  /**
   * {@inheritDoc}
   */
  public function __construct(ProductInterface $product, string $format, array $context) {
    parent::__construct($format, $context);
    $this->product = $product;
  }

  /**
   * Get the product.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   */
  public function getProduct(): ProductInterface {
    return $this->product;
  }

  /**
   * Get the format.
   *
   * @return string
   */
  public function getFormat(): string {
    return $this->format;
  }

  /**
   * Get the context.
   *
   * @return array
   */
  public function getContext(): array {
    return $this->context;
  }

  /**
   * Get the variations.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   */
  public function getVariations(): array {
    return $this->variations;
  }

  /**
   * Set variations.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface[] $variations
   */
  public function setVariations(array $variations): void {
    $this->variations = $variations;
  }

}
