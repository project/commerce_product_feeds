<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Event;

use Drupal\Component\Assertion\Inspector;
use Symfony\Component\EventDispatcher\Event;

class ProductSelectionEvent extends FeedDataSelectionEventBase {

  /**
   * Products to generate feed.
   *
   * @var string[]
   */
  protected $products;

  /**
   * Get product IDs.
   *
   * @return string[]
   */
  public function getProductIds(): array {
    return $this->products;
  }

  /**
   * Setter for products.
   *
   * @param string[] $productIds
   *   Product IDs.
   */
  public function setProductIds(array $productIds): void {
    Inspector::assertAllStringable($productIds);
    $this->products = $productIds;
  }

}
