<?php

namespace Drupal\commerce_product_feeds\Serializer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

/**
 * Overrides the Symfony serializer to provide fallback serialization, e.g.
 * for typed data.
 *
 * @see \Drupal\jsonapi\Serializer\Serializer
 */
final class Serializer extends SymfonySerializer {

  /**
   * A normalizer to fall back on when we cannot normalize an object.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface
   */
  protected $fallbackNormalizer;

  /**
   * Adds a secondary normalizer.
   *
   * This normalizer will be attempted when JSON:API has no applicable
   * normalizer.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $normalizer
   *   The secondary normalizer.
   */
  public function setFallbackNormalizer(NormalizerInterface $normalizer) {
    $this->fallbackNormalizer = $normalizer;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($data, $format = NULL, array $context = []) {
    if ($this->selfSupportsNormalization($data, $format, $context)) {
      return parent::normalize($data, $format, $context);
    }
    if ($this->fallbackNormalizer->supportsNormalization($data, $format, $context)) {
      return $this->fallbackNormalizer->normalize($data, $format, $context);
    }
    return parent::normalize($data, $format, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL, array $context = []) {
    return $this->selfSupportsNormalization($data, $format, $context) || $this->fallbackNormalizer->supportsNormalization($data, $format, $context);
  }

  /**
   * Checks whether this class alone supports normalization.
   *
   * @param mixed $data
   *   Data to normalize.
   * @param string $format
   *   The format being (de-)serialized from or into.
   * @param array $context
   *   (optional) Options available to the normalizer.
   *
   * @return bool
   *   Whether this class supports normalization for the given data.
   */
  private function selfSupportsNormalization($data, $format = NULL, array $context = []) {
    return parent::supportsNormalization($data, $format, $context);
  }

}
