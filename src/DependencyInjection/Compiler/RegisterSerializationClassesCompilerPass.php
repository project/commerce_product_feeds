<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\DependencyInjection\Compiler;

use Drupal\serialization\RegisterSerializationClassesCompilerPass as DrupalRegisterSerializationClassesCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RegisterSerializationClassesCompilerPass extends DrupalRegisterSerializationClassesCompilerPass {

  /**
   * The service ID.
   *
   * @const string
   */
  const OVERRIDDEN_SERVICE_ID = 'commerce_product_feeds.serializer';

  /**
   * The service tag that only product feeds normalizers should use.
   *
   * @const string
   */
  const OVERRIDDEN_SERVICE_NORMALIZER_TAG = 'commerce_product_feeds_normalizer';

  /**
   * The service tag that only product feeds encoders should use.
   *
   * @const string
   */
  const OVERRIDDEN_SERVICE_ENCODER_TAG = 'commerce_product_feeds_encoder';

  /**
   * Adds services to the Commerce Product Feeds Serializer.
   *
   * This code is copied from the class parent with two modifications. The
   * service id has been changed and the service tag has been updated.
   *
   * ID: 'serializer' -> 'commerce_product_feeds.serializer'
   * Tag: 'normalizer' -> 'commerce_product_feeds_normalizer'
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *   The container to process.
   */
  public function process(ContainerBuilder $container) {
    $definition = $container->getDefinition(static::OVERRIDDEN_SERVICE_ID);

    // Retrieve registered Normalizers and Encoders from the container.
    foreach ($container->findTaggedServiceIds(static::OVERRIDDEN_SERVICE_NORMALIZER_TAG) as $id => $attributes) {
      $priority = isset($attributes[0]['priority']) ? $attributes[0]['priority'] : 0;
      $normalizers[$priority][] = new Reference($id);
    }
    foreach ($container->findTaggedServiceIds(static::OVERRIDDEN_SERVICE_ENCODER_TAG) as $id => $attributes) {
      $priority = isset($attributes[0]['priority']) ? $attributes[0]['priority'] : 0;
      $encoders[$priority][] = new Reference($id);
    }

    // Add the registered Normalizers and Encoders to the Serializer.
    if (!empty($normalizers)) {
      $definition->replaceArgument(0, $this->sort($normalizers));
    }
    if (!empty($encoders)) {
      $definition->replaceArgument(1, $this->sort($encoders));
    }

  }

}
