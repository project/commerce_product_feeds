<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds;

use Drupal\commerce_product\Entity\Product;

class ProductCollection implements \IteratorAggregate, \Countable {

  /**
   * Commerce product IDs to normalize into a feed.
   *
   * @var string[]
   */
  protected $data;

  /**
   * Title for XML feed.
   *
   * @var string|NULL
   */
  protected $title;

  /**
   * Link for XML feed.
   *
   * @var string|NULL
   */
  protected $link;

  /**
   * Instantiates a ProductCollection object.
   *
   * @param string[] $data
   *   The commerce product IDs for the collection.
   */
  public function __construct(array $data, ?string $title = NULL, ?string $link = NULL) {
    $this->data = array_values($data);
    $this->title = $title;
    $this->link = $link;
  }

  /**
   * Returns an iterator for entities.
   *
   * @return \Traversable
   *   Generator.
   */
  public function getIterator() {
    foreach ($this->data as $id) {
      yield Product::load($id);
    }
  }

  /**
   * Returns the number of products.
   *
   * @return int
   *   The number of products.
   */
  public function count() {
    return count($this->data);
  }

  /**
   * Returns the collection as an array.
   *
   * @return string[]
   *   The array of product IDs.
   */
  public function toArray() {
    return $this->data;
  }

  /**
   * Returns a new ProductCollection object containing the entities of $this and $other.
   *
   * @param \Drupal\commerce_product_feeds\ProductCollection $a
   *   A ProductCollection object object to be merged.
   * @param \Drupal\commerce_product_feeds\ProductCollection $b
   *   A ProductCollection object to be merged.
   *
   * @return static
   *   A new merged ProductCollection object.
   */
  public static function merge(ProductCollection $a, ProductCollection $b) {
    return new static(array_merge($a->toArray(), $b->toArray()));
  }

  /**
   * Returns a new, deduplicated Data object.
   *
   * @param \Drupal\commerce_product_feeds\ProductCollection $collection
   *   The ProductCollection object to deduplicate.
   *
   * @return static
   *   A new merged Data object.
   */
  public static function deduplicate(ProductCollection $collection) {
    $deduplicated = [];
    foreach ($collection->toArray() as $resource) {
      $deduplicated[$resource] = $resource;
    }
    return new static(array_values($deduplicated));
  }

  /**
   * Getter for title.
   *
   * @return string
   */
  public function getTitle(): string {
    return $this->title ?? '';
  }

  /**
   * Getter for link.
   *
   * @return string
   */
  public function getLink(): string {
    return $this->link ?? '';
  }

}
