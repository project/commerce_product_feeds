<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer;

use Drupal\commerce_price\CurrencyFormatter;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_feeds\Event\ProductVariationNormalizeEvent;
use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Normalizer for commerce product variations.
 */
class CommerceProductVariationNormalizer extends CommerceProductFeedsCachingNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ProductVariationInterface::class;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Currency formatter.
   *
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * @inheritDoc
   */
  public function __construct(
    NormalizedValueCacher $cacher,
    EventDispatcherInterface $eventDispatcher,
    CurrencyFormatter $currencyFormatter,
    DateFormatterInterface $dateFormatter
  ) {
    parent::__construct($cacher);
    $this->eventDispatcher = $eventDispatcher;
    $this->currencyFormatter = $currencyFormatter;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * @inheritDoc
   */
  protected function getNormalization($object, $format, $context): CacheableNormalization {
    $this->assertNormalizerObject($object);
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $object */
    $price = $object->getPrice();
    $value = [
      // Atom required values.
      'id' => $object->toUrl()->setAbsolute()->toString(TRUE)
        ->getGeneratedUrl(),
      'updated' => $this->dateFormatter
        ->format($object->getChangedTime(), 'custom', \DateTime::ATOM),
      'title' => $object->label(),
      // Namespaced values.
      'g:id' => $object->id(),
      'g:title' => $object->label(),
      'g:link' => $object->toUrl()->setAbsolute()->toString(TRUE)
        ->getGeneratedUrl(),
      'g:price' => sprintf(
        '%s %s',
        $this->currencyFormatter->format(
          $price->getNumber(), $price->getCurrencyCode(),
          [
            'use_grouping' => FALSE,
            'currency_display' => 'none',
          ]
        ),
        $price->getCurrencyCode()
      ),
    ];
    $event = new ProductVariationNormalizeEvent($object, $this->serializer, $value, $context);
    $this->eventDispatcher
      ->dispatch('commerce_product_feeds.product_variation_normalize', $event);
    $event->addCacheableDependency($object);
    $value = $event->getValue();
    Inspector::assertAllStrings($value);
    return (new CacheableNormalization($event, $value))->omitIfEmpty()
      ->addCacheTags([$object->getEntityTypeId() . '_feeds_normalization']);
  }

}
