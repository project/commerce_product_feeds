<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer\Cache;

use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableOmission;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Service to wrap gets and sets to the normalized value cache.
 */
class NormalizedValueCacher implements NormalizedValueCacherInterface {

  /**
   * Contexts to ignore when storing normalizations.
   */
  const IGNORED_CONTEXTS = [
    'url.query_args:v',
    'store'
  ];

  /**
   * Cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBin;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBin
   *   Cache backend.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(CacheBackendInterface $cacheBin, TimeInterface $time) {
    $this->cacheBin = $cacheBin;
    $this->time = $time;
  }

  /**
   * {@inheritDoc}
   */
  public function get(ContentEntityInterface $entity): ?CacheableNormalization {
    $cached = $this->cacheBin->get($this->getCid($entity));
    return $cached
      ? new CacheableNormalization((new CacheableMetadata())->setCacheTags($cached->tags), $cached->data)
      : NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function set(CacheableNormalization $object, ContentEntityInterface $entity): void {
    // Use cacheability metadata from the cacheable normalization only - it
    // should already have the content entity added as a cacheable dependency
    // and also includes additional metadata such as the custom feeds cache
    // tags.
    //
    // @see \Drupal\commerce_product_feeds\Normalizer\CommerceProductFeedsCachingNormalizerBase::getNormalization()
    // @see \Drupal\commerce_product_feeds\Normalizer\CommerceProductVariationNormalizer::getNormalization()

    $remainingContexts = array_diff(
      $object->getCacheContexts(),
      self::IGNORED_CONTEXTS
    );
    assert(
      ($object instanceof CacheableOmission) || (count($remainingContexts) === 0),
      new \RuntimeException(
        sprintf('Cache contexts for normalization to product feeds not [yet] supported. Saw: %s', implode(', ', $remainingContexts))
      )
    );
    $expire = $object->getCacheMaxAge();
    if ($expire !== Cache::PERMANENT) {
      // Convert from an age to a unix timestamp.
      $expire += $this->time->getRequestTime();
    }
    $this->cacheBin->set(
      $this->getCid($entity),
      ($object instanceof CacheableOmission) ? [] : $object->getNormalization(),
      $expire,
      $object->getCacheTags()
    );
  }

  /**
   * Get a cache ID for an object.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $object
   *   Entity to retrieve cache ID for.
   *
   * @return string
   */
  protected function getCid(ContentEntityInterface $object): string {
    return $object->getEntityTypeId() . ':' . $object->id();
  }

  /**
   * @inheritDoc
   */
  public function clearCache(): void {
    $this->cacheBin->deleteAll();
  }

}
