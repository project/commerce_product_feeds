<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer\Cache;

use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface for normalization cachers.
 */
interface NormalizedValueCacherInterface {

  /**
   * Reads an entity normalization from cache.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface
   *   The entity object for which to retrieve a cache item.
   *
   * @return \Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization
   *   Cached normalization.
   *
   * @see \Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber::renderArrayToResponse()
   */
  public function get(ContentEntityInterface $entity): ?CacheableNormalization;

  /**
   * Writes a normalization to cache.
   *
   * @param \Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization $object
   *   The normalization.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity (to extract the cache ID)
   */
  public function set(CacheableNormalization $object, ContentEntityInterface $entity): void;

  /**
   * Clear the cache.
   */
  public function clearCache(): void;

}
