<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer;

use DateTime;
use Drupal\commerce_product_feeds\Normalizer\Value\Normalization;
use Drupal\commerce_product_feeds\ProductCollection;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;

/**
 * Normalizer for a collection of commerce products.
 */
class CommerceProductCollectionNormalizer extends CommerceProductFeedsNormalizerBase {

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ProductCollection::class;

  /**
   * @inheritDoc
   */
  public function __construct(DateFormatterInterface $dateFormatter, TimeInterface $time) {
    $this->dateFormatter = $dateFormatter;
    $this->time = $time;
  }

  /**
   * {@inheritDoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $this->assertNormalizerObject($object);
    /** @var \Drupal\commerce_product_feeds\ProductCollection $object */
    // The iterator is a generator function.
    $values = [];
    foreach ($object->getIterator() as $product) {
      $values[] = $this->serializer->normalize($product, $format, $context);
    }
    $normalization = Normalization::aggregatedMerged($values)->getNormalization();
    // @see https://validator.w3.org/feed/docs/atom.html#requiredFeedElements
    return array_merge(
      [
        'title' => $object->getTitle(),
        'id' => $object->getLink(),
        'link' => ['@rel' => 'self', '@href' => $object->getLink()],
        'updated' => $this->dateFormatter
          ->format($this->time->getRequestTime(), 'custom', DateTime::ATOM),
        'generator' => 'Drupal Commerce',
      ],
      array_filter(['entry' => $normalization])
    );
  }

}
