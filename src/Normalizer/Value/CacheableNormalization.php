<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer\Value;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;

/**
 * Use to store normalized data and its cacheability.
 *
 * Largely inspired by the json:api implementation; however we do not need to
 * utilize the render cache as we are less dependent on bubbleable context,
 * and hopefully the VariationCache will land in 9.x sooner rather than later
 * making cache redirects natively supported across all types of bins.
 *
 * @see \Drupal\jsonapi\Normalizer\Value\CacheableNormalization
 */
class CacheableNormalization extends Normalization implements RefinableCacheableDependencyInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * CacheableNormalization constructor.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $cacheability
   *   The cacheability metadata for the normalized data.
   * @param array|string|int|float|bool|null $normalization
   *   The normalized data. This value must not contain any
   *   CacheableNormalizations.
   */
  public function __construct(CacheableDependencyInterface $cacheability, $normalization) {
    parent::__construct($normalization);
    assert(static::hasNoNestedInstances($normalization));
    $this->setCacheability($cacheability);
  }

  /**
   * Creates a CacheableNormalization instance without any special cacheability.
   *
   * @param array|string|int|float|bool|null $normalization
   *   The normalized data. This value must not contain any
   *   CacheableNormalizations.
   *
   * @return \Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization
   *   The CacheableNormalization.
   */
  public static function permanent($normalization) {
    return new static(new CacheableMetadata(), $normalization);
  }

  /**
   * Converts the object to a CacheableOmission if the normalization is empty.
   *
   * @return self|\Drupal\commerce_product_feeds\Normalizer\Value\CacheableOmission
   *   A CacheableOmission if the normalization is considered empty, self
   *   otherwise.
   */
  public function omitIfEmpty() {
    return empty($this->normalization) ? new CacheableOmission($this) : $this;
  }

  /**
   * Gets a new CacheableNormalization with an additional dependency.
   *
   * @param \Drupal\Core\Cache\CacheableDependencyInterface $dependency
   *   The new cacheable dependency.
   *
   * @return static
   *   A new object based on the current value with an additional cacheable
   *   dependency.
   */
  public function withCacheableDependency(CacheableDependencyInterface $dependency) {
    return new static(CacheableMetadata::createFromObject($this)->addCacheableDependency($dependency), $this->normalization);
  }

  /**
   * Collects an array of CacheableNormalizations into a single instance.
   *
   * @param \Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization[] $cacheable_normalizations
   *   An array of CacheableNormalizations.
   *
   * @return static
   *   A new CacheableNormalization. Each input value's cacheability will be
   *   merged into the return value's cacheability. The return value's
   *   normalization will be an array of the input's normalizations. This method
   *   does *not* behave like array_merge() or NestedArray::mergeDeep().
   */
  public static function aggregate(array $cacheable_normalizations) {
    assert(Inspector::assertAllObjects($cacheable_normalizations, CacheableNormalization::class));
    return new static(
      array_reduce($cacheable_normalizations, function (CacheableMetadata $merged, CacheableNormalization $item) {
        return $merged->addCacheableDependency($item);
      }, new CacheableMetadata()),
      array_reduce(array_keys($cacheable_normalizations), function ($merged, $key) use ($cacheable_normalizations) {
        if (!$cacheable_normalizations[$key] instanceof CacheableOmission) {
          $merged[$key] = $cacheable_normalizations[$key]->getNormalization();
        }
        return $merged;
      }, [])
    );
  }

  /**
   * Ensures that no nested values are instances of this class.
   *
   * @param array|\Traversable $array
   *   The traversable object which may contain instance of this object.
   *
   * @return bool
   *   Whether the given object or its children have CacheableNormalizations in
   *   them.
   */
  protected static function hasNoNestedInstances($array) {
    foreach ($array as $value) {
      if ((is_array($value) || $value instanceof \Traversable) && !static::hasNoNestedInstances($value) || $value instanceof static) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
