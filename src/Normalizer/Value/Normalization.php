<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer\Value;

use Drupal\Component\Assertion\Inspector;

/**
 * Wrapper around normalization(s).
 */
class Normalization {

  /**
   * A normalized value.
   *
   * @var mixed
   */
  protected $normalization;

  /**
   * Constructor.
   *
   * @param array|string|int|float|bool|null $normalization
   *   The normalized data. This value must not contain any
   *   CacheableNormalizations.
   */
  public function __construct($normalization) {
    assert(is_array($normalization) || is_string($normalization) || is_int($normalization) || is_float($normalization) || is_bool($normalization) || is_null($normalization));
    $this->normalization = $normalization;
  }

  /**
   * Gets the decorated normalization.
   *
   * @return array|string|int|float|bool|null
   *   The normalization.
   */
  public function getNormalization() {
    return $this->normalization;
  }

  /**
   * Collects an array of Normalizations into a single instance, flattened with
   * all the passed normalization values combined with array_merge().
   *
   * This does not collect cacheability data; e.g., when this is top-level
   * and no longer necessary.
   *
   * @param \Drupal\commerce_product_feeds\Normalizer\Value\Normalization[] normalizations
   *   An array of Normalizations.
   *
   * @return static
   *   A new Normalization. The return value's normalization will be an array
   *   of the input normalizations' values flattened into a single array with
   *   array_merge().
   */
  public static function aggregatedMerged(array $normalizations) {
    assert(Inspector::assertAllObjects($normalizations, Normalization::class));
    return new static(
      array_reduce(array_keys($normalizations), function ($merged, $key) use ($normalizations) {
        if (!$normalizations[$key] instanceof CacheableOmission) {
          $merged = array_merge($merged, $normalizations[$key]->getNormalization());
        }
        return $merged;
      }, [])
    );
  }
}
