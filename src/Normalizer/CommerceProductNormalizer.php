<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product_feeds\Event\ProductVariationSelectionEvent;
use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Normalizer for commerce products.
 */
class CommerceProductNormalizer extends CommerceProductFeedsCachingNormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ProductInterface::class;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @inheritDoc
   */
  public function __construct(NormalizedValueCacher $cacher, EventDispatcherInterface $eventDispatcher, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($cacher);
    $this->eventDispatcher = $eventDispatcher;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  protected function getNormalization($object, $format, $context): CacheableNormalization {
    /** @var \Drupal\commerce_product\Entity\ProductInterface $object */
    $variations = [];
    foreach ($this->getVariations($object, $format, $context) as $variation) {
      $variations[] = $this->serializer->normalize($variation, $format, $context);
    }
    return CacheableNormalization::aggregate($variations)
      ->omitIfEmpty()
      ->withCacheableDependency($object)
      ->addCacheTags([$object->getEntityTypeId() . '_feeds_normalization']);
  }

  /**
   * Get product variations for the product; allow event subscribers to
   * influence this decision.
   *
   * If no subscribers exist, the default is to return all variations.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   Product to filter.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   */
  protected function getVariations(ProductInterface $product, $format, $context): array {
    // @see https://www.drupal.org/project/drupal/issues/2825358
    $eventName = 'commerce_product_feeds.product_variation_selection';
    if (!$this->eventDispatcher->hasListeners($eventName)) {
      /** @var \Drupal\commerce_product\ProductVariationStorageInterface $variationStorage */
      $variationStorage = $this->entityTypeManager
        ->getStorage('commerce_product_variation');
      return $variationStorage->loadEnabled($product);
    }
    $event = new ProductVariationSelectionEvent($product, $format, $context);
    $this->eventDispatcher->dispatch($eventName, $event);
    return $event->getVariations();
  }

}
