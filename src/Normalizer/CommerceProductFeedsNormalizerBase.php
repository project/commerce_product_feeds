<?php

namespace Drupal\commerce_product_feeds\Normalizer;

use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Base class for normalizers involved in producing a commerce product feed.
 */
abstract class CommerceProductFeedsNormalizerBase extends NormalizerBase {

  /**
   * {@inheritDoc}
   */
  protected $format = ['xml_atom'];

  /**
   * Asserts object is supported at runtime.
   *
   * @param mixed $object
   *   Object to validate.
   */
  protected function assertNormalizerObject($object) {
    assert($object instanceof $this->supportedInterfaceOrClass, new \InvalidArgumentException());
  }

}
