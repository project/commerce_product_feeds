<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Normalizer;

use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher;
use Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Base caching normalizer.
 */
abstract class CommerceProductFeedsCachingNormalizerBase extends CommerceProductFeedsNormalizerBase {

  /**
   * Value cacher.
   *
   * @var \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher
   */
  protected $cacher;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacher $cacher
   */
  public function __construct(NormalizedValueCacher $cacher) {
    $this->cacher = $cacher;
  }

  /**
   * {@inheritDoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    // This method returns a CacheableNormalization, inconsistent with the
    // interface docblock. Similar to what jsonapi module does. The root
    // normalizer returns a proper value.
    $this->assertNormalizerObject($object);
    /** @var \Drupal\commerce_product\Entity\ProductInterface $object */
    if ($cached = $this->cacher->get($object)) {
      return $cached;
    }
    $normalization = $this->getNormalization($object, $format, $context);
    $this->cacher->set($normalization, $object);
    return $normalization;
  }

  /**
   * Entity-specific normalization.
   *
   * @param $object
   *   Content entity to normalize.
   * @param string $format
   *   Format the normalization result will be encoded as.
   * @param array $context
   *   Context options for the normalizer.
   *
   * @return \Drupal\commerce_product_feeds\Normalizer\Value\CacheableNormalization
   *   Normalization.
   */
  abstract protected function getNormalization($object, $format, $context): CacheableNormalization;

}
