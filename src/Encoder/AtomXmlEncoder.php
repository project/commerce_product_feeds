<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Encoder;

use Drupal\serialization\Encoder\XmlEncoder;

class AtomXmlEncoder extends XmlEncoder {

  /**
   * {@inheritDoc}
   */
  protected static $format = ['xml_atom'];

  /**
   * @inheritDoc
   */
  public function encode($data, $format, array $context = []) {
    $context += [
      'xml_root_node_name' => 'feed',
    ];
    $data += [
      '@xmlns:g' => 'http://base.google.com/ns/1.0',
      '@xmlns' => 'http://www.w3.org/2005/Atom',
    ];
    return parent::encode($data, $format, $context);
  }

}
