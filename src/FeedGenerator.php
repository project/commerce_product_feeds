<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds;

use Drupal\commerce_product_feeds\Event\ProductSelectionEvent;
use Drupal\commerce_product_feeds\Serializer\Serializer;
use Drupal\Component\Utility\Timer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FeedGenerator implements FeedGeneratorInterface {

  /**
   * The name of the timer used for tracking generation.
   *
   * @var string
   */
  private const TIMER_NAME = 'commerce_product_feeds_generator';

  /**
   * Serializer.
   *
   * @var \Drupal\commerce_product_feeds\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_product_feeds\Serializer\Serializer $serializer
   *   Serializer.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $accountSwitcher
   *   The account switcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(
    Serializer $serializer,
    EventDispatcherInterface $eventDispatcher,
    EntityTypeManagerInterface $entityTypeManager,
    AccountSwitcherInterface $accountSwitcher,
    ConfigFactoryInterface $configFactory,
    LoggerInterface $logger
  ) {
    $this->serializer = $serializer;
    $this->eventDispatcher = $eventDispatcher;
    $this->entityTypeManager = $entityTypeManager;
    $this->accountSwitcher = $accountSwitcher;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function Generate(string $format, array $context = [], ?string $title = NULL, ?string $link = NULL): string {
    Timer::start(static::TIMER_NAME);

    $products = $this->getProductIds($format, $context);
    $title = $title ?? $this->configFactory->get('system.site')->get('name');
    $link = $link
      ?? Url::fromRoute('<front>')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
    $collection = new ProductCollection($products, $title, $link);
    $serialized = $this->serializer->serialize($collection, $format, $context);

    Timer::stop(static::TIMER_NAME);
    $log_context = [
      '@count' => count($products),
      '@format' => $format,
      '@time' => Timer::read(static::TIMER_NAME),
    ];
    $this->logger->notice("Generated @format feed with @count products in @time ms.", $log_context);

    return $serialized;
  }

  /**
   * Get products to generate the data feed.
   *
   * If no subscribers exist, the default is to return all products.
   *
   * @param string $format
   *   Format the feed will be serialized as.
   * @param array $context
   *   Context options for the serializer.
   *
   * @return string[]
   *   Product IDs.
   */
  protected function getProductIds($format, $context): array {
    // @see https://www.drupal.org/project/drupal/issues/2825358
    $eventName = 'commerce_product_feeds.product_selection';
    if (!$this->eventDispatcher->hasListeners($eventName)) {
      $productStorage = $this->entityTypeManager
        ->getStorage('commerce_product');
      // Commerce products have access control implemented by Entity API's
      // entity query access feature. This is then executed in the context of
      // the anonymous user.
      $this->accountSwitcher->switchTo(new AnonymousUserSession());
      $ids = $productStorage->getQuery()->execute();
      $this->accountSwitcher->switchBack();
      return $ids;
    }
    $event = new ProductSelectionEvent($format, $context);
    $this->eventDispatcher->dispatch($eventName, $event);
    return $event->getProductIds();
  }

}
