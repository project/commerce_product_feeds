<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds\Commands;

use Drupal\commerce_product_feeds\FeedGeneratorInterface;
use Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacherInterface;
use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;
use Psr\Log\LoggerInterface;

/**
 * Drush commands.
 */
class GenerationCommands extends DrushCommands {

  /**
   * Feed generator.
   *
   * @var \Drupal\commerce_product_feeds\FeedGeneratorInterface
   */
  protected $feedGenerator;

  /**
   * The Commerce Product Feeds channel logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $feedLogger;

  /**
   * File System.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Cacher.
   *
   * @var \Drupal\commerce_product_feeds\Normalizer\Cache\NormalizedValueCacherInterface
   */
  protected $cacher;

  /**
   * {@inheritDoc}
   */
  public function __construct(FeedGeneratorInterface $feedGenerator, FileSystemInterface $fileSystem, NormalizedValueCacherInterface $cacher, LoggerInterface $feedLogger) {
    parent::__construct();
    $this->feedGenerator = $feedGenerator;
    $this->feedLogger = $feedLogger;
    $this->fileSystem = $fileSystem;
    $this->cacher = $cacher;
  }

  /**
   * The product-feed:generate command.
   *
   * This command generates a product feed in a specified format and outputs
   * the result to a file. The underlying logic is contained in the generator
   * service.
   *
   * @param string $filename
   *   Filename to output generated feed.
   *
   * @command product-feed:generate
   * @option string $format Format for the feed.
   */
  public function generate(string $filename, $options = ['format' => 'xml_atom']): void {
    $context = ['options' => ['drush' => $options]];
    $output = $this->feedGenerator->generate($options['format'], $context);
    $this->fileSystem
      ->saveData($output, $filename, FileSystemInterface::EXISTS_REPLACE);

    $context = [
      '@filename' => $filename,
      '@memory' => format_size(memory_get_peak_usage()),
    ];
    $this->feedLogger->notice("Created product feed @filename. Peak memory usage @memory.", $context);
  }

  /**
   * Alter cache types for clearing with drush cache:clear.
   *
   * @hook on-event cache-clear
   */
  public function alterCacheTypesToClear(array &$types) {
    $types['commerce-product-feeds'] = [$this->cacher, 'clearCache'];
  }

}
