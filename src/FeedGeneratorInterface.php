<?php declare(strict_types=1);

namespace Drupal\commerce_product_feeds;

/**
 * Interface for product feed generators.
 */
interface FeedGeneratorInterface {

  /**
   * Generate the product feed in a given format.
   *
   * @param string $format
   *   Format for serialization; e.g., 'xml_atom'.
   * @param array $context
   *   Serialization context.
   * @param string|NULL $title
   *   Title for feed, if the format requires it.
   * @param string|NULL $link
   *   Link for feed, if the format requires it.
   *
   * @return string
   *   Serialized data feed.
   */
  public function generate(string $format, array $context = [], ?string $title = NULL, ?string $link = NULL): string;

}
