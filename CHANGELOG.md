# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.1.1] - 2022-05-19

### Fixed
- [#3281485]: Fix tests for Drupal 9.

## [2.1.0] - 2022-05-12

### Added
- [#3128962]: Support Drupal 9.

## [2.0.0] - 2021-10-04

### Fixed
- [#3240316]: The normalization cache incorrectly interprets the max age.
- [#3240506]: Cache tags from cacheable normalizations are ignored.

### Changed
- Switch to semantic versioning. The major version bump is just to avoid
  ambiguity with the old style branch.

### Added
- [#3226759]: Pass drush command options as normalization context.
- [#3165114]: Log details of feed generation/export.
- [#3229969]: Support custom cacheability metadata for variation normalizations.

## [8.x-1.0-beta1] - 2020-04-16

[#3128962]: https://www.drupal.org/project/commerce_product_feeds/issues/3128962
[#3165114]: https://www.drupal.org/project/commerce_product_feeds/issues/3165114
[#3226759]: https://www.drupal.org/project/commerce_product_feeds/issues/3226759
[#3229969]: https://www.drupal.org/project/commerce_product_feeds/issues/3229969
[#3240316]: https://www.drupal.org/project/commerce_product_feeds/issues/3240316
[#3240506]: https://www.drupal.org/project/commerce_product_feeds/issues/3240506
[#3281485]: https://www.drupal.org/project/commerce_product_feeds/issues/3281485

[Unreleased]: https://git.drupalcode.org/project/commerce_product_feeds/-/compare/2.1.1...2.x
[2.1.1]: https://git.drupalcode.org/project/commerce_product_feeds/-/compare/2.1.0...2.1.1
[2.1.0]: https://git.drupalcode.org/project/commerce_product_feeds/-/compare/2.0.0...2.1.0
[2.0.0]: https://git.drupalcode.org/project/commerce_product_feeds/-/compare/8.x-1.0-beta1...2.0.0
[8.x-1.0-beta1]: https://git.drupalcode.org/project/commerce_product_feeds/tree/8.x-1.0-beta1
